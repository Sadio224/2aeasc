import React, { Component } from 'react'

import {  Link } from "react-router-dom"
import Footer from '../helpers/Footer'

class PageWrapper extends Component{

    render(){

        return(
            <div className="site-wrap">
                <div className="site-mobile-menu site-navbar-target">
                    <div className="site-mobile-menu-header">
                        <div className="site-mobile-menu-close mt-3">
                           <span className="icon-close2 js-menu-toggle"></span>
                        </div>
                    </div>
                    <div className="site-mobile-menu-body"></div>
                </div>

                <div className="py-2 bg-light">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-9 d-none d-lg-block">
                                <Link to="/#" className="small mr-3"><span className="icon-globe mr-2"></span> Avenue du college, rue 5 Guinée</Link> 
                                <Link to="/#" className="small mr-3"><span className="icon-phone2 mr-2"></span> +224 626 68 43 92</Link> 
                                <Link to="/#" className="small mr-3"><span className="icon-envelope-o mr-2"></span> collegesangoyah@gmail.com</Link> 
                            </div>
                            <div className="col-lg-3 text-right">
                                <Link to="/login" className="small mr-3"><span className="icon-unlock-alt"></span> Se connecter</Link>
                                <Link to="/register" className="small btn btn-primary px-4 py-2 rounded-0"><span className="icon-users"></span> Adhérer</Link>
                            </div>
                        </div>
                    </div>
                </div>

                <header className="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">

                    <div className="container">
                        <div className="d-flex align-items-center">
                            <div className="site-logo">
                                <Link to="/" className="d-block">
                                    <img src="images/logo2.jpeg" alt="Image1" className="img-fluid" />
                                </Link>
                            </div>
                            <div className="mr-auto">
                                <nav className="site-navigation position-relative text-right" role="navigation">
                                    <ul className="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                                        <li className="active">
                                        <Link to="/" className="nav-link text-left">Accueil</Link>
                                        </li>
                                        <li className="has-children">
                                        <Link to="/about" className="nav-link text-left">2AE2SC</Link>
                                        <ul className="dropdown">
                                            <li><Link to="/about">Presentation</Link></li>
                                            <li><Link to="/mot_president">Mot du President</Link></li>
                                            <li><Link to="/#">Organigramme</Link></li>
                                            <li><Link to="/membre">Membre</Link></li>
                                            <li><Link to="/register">Adhésion</Link></li>
                                        </ul>
                                        </li>
                                        <li>
                                        <Link to="/admissions" className="nav-link text-left">Nos Projets</Link>
                                        </li>
                                        <li className="has-children">
                                        <Link to="/#" className="nav-link text-left">College Sangoyah</Link>
                                        <ul className="dropdown">
                                            <li><Link to="/#">Administration</Link></li>
                                            <li><Link to="/courses">Cours</Link></li>
                                            <li><Link to="/#">Programme </Link></li>
                                        </ul>
                                        </li>
                                        <li>
                                        <Link to="/contact" className="nav-link text-left">Contact</Link>
                                        </li>
                                    </ul>                                                                                                                                                                                                                                                                                       
                                </nav>
                            </div>
                            <div className="ml-auto">
                                <div className="social-wrap">
                                    <Link to="/#"><span className="icon-facebook"></span></Link>
                                    <Link to="/#"><span className="icon-youtube"></span></Link>
                                    <Link to="/#"><span className="icon-linkedin"></span></Link>

                                    <Link to="/#" className="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black">
                                    <span className="icon-menu h3"></span></Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                {this.props.children}
                <Footer/>
            </div>
            
        )
    }
}

export default PageWrapper;