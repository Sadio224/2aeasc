import React, {Component} from 'react'

class Carosel extends Component{

    render(){
        return(
            <div>
                <div className="hero-slide owl-carousel site-blocks-cover">
                    <div className="intro-section" style={{backgroundImage: "url(" + this.props.image + ")" }} >
                        <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-12 mx-auto text-center" data-aos="fade-up">
                            <h1>College Sangoyah</h1>
                            </div>
                        </div>
                        </div>
                    </div>

                    <div className="intro-section" style={{backgroundImage: "url(" + this.props.image + ")" }} >
                        <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-12 mx-auto text-center" data-aos="fade-up">
                            <h1>2019-2020</h1>
                            </div>
                        </div>
                        </div>
                    </div>

                    <div className="intro-section " style={{backgroundImage: "url(" + this.props.image + ")"}} >
                        <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-12 mx-auto text-center" data-aos="fade-up">
                            <h2>Association des anciens éleves du college sangoyah</h2>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Carosel;