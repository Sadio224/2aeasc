import React from 'react'
import {Link} from 'react-router-dom'

const Footer = () => (
    <div className="footer">
      <div className="container">
        <div className="row">
          <div className="col-lg-4">
            <p className="mb-4"><img src="images/logo2.jpeg" alt="Image1" className="img-fluid"/></p>
            <p>Il est fondé entre les adhérents aux présents statuts une Organisation Non Gouvernementale (ONG) à but non lucratif, apolitique et régie par les lois en vigueur en République de Guinée.</p>  
            <p><Link to="/">Lire Plus</Link></p>
          </div>
          <div className="col-lg-3">
            <h3 className="footer-heading"><span>Ressource pedagogique</span></h3>
            <ul className="list-unstyled">
                <li><Link to="/">Cours magistraux</Link ></li>
                <li><Link to="/">Examens et controles</Link ></li>
                <li><Link to="/">Traveaux diriges et pratique</Link ></li>
            </ul>
          </div>
          <div className="col-lg-2">
              <h3 className="footer-heading"><span>ressource humaine</span></h3>
              <ul className="list-unstyled">
                  <li><Link to="/">Administration</Link ></li>
                  <li><Link to="/">Professeurs</Link ></li>
                  <li><Link to="/">Bureau des etudiants(BDE)</Link ></li>
              </ul>
          </div>
          <div className="col-lg-3">
              <h3 className="footer-heading"><span>Contact</span></h3>
              <ul className="list-unstyled">
                  <li><Link to="/">Help Center</Link ></li>
                  <li><Link to="/">Support Community</Link ></li>
                  <li><Link to="/">Press</Link ></li>
                  <li><Link to="/">Share Your Story</Link ></li>
                  <li><Link to="/">Our Supporters</Link ></li>
              </ul>
          </div>
        </div>

        <div className="row">
          <div className="col-12">
            <div className="copyright">
                <p>  
                  Copyright &copy;<script>document.write(new Date().getFullYear());</script> Tout droit reservé | Ce site a été developpé avec amour <i className="icon-heart" aria-hidden="true"></i> par <Link to="https://www.xyzsociety.com" target="_blank" >XYZ.society</Link >    
                </p>
            </div>
          </div>
        </div>
      </div>
    </div>
)

export default Footer;