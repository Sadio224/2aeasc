import React from 'react'

const Bureau = ({image, nom, poste, profession}) =>(
    <div className="col-lg-4 col-md-6 mb-5 mb-lg-5">
        <div className="feature-1 border person text-center">
            <img src={image} alt="Image1" className="img-fluid" />
            <div className="feature-1-content">
                <h2>{nom}</h2>
                <span className="position mb-3 d-block">{poste}</span>    
                <p>{profession}</p>
            </div>
        </div>
    </div>
)

export default Bureau;