import React from 'react'

const Abonnement = () =>(
    <div className="site-section ftco-subscribe-1" style={{backgroundImage: "url( images/bg_1.jpg )"}}>
        <div className="container">
            <div className="row align-items-center">
                <div className="col-lg-7">
                    <h2>Soyez Informés De Nos Activités!</h2>
                    <p>Ne ratez aucun événement, recevez par courier électronique un rappel de toutes nos actions e temps réels</p>
                </div>
                <div className="col-lg-5">
                    <form action="" className="d-flex">
                        <input type="email" className="rounded form-control mr-2 py-3" placeholder="Enter your email"/>
                    <button className="btn btn-primary rounded py-3 px-4" type="submit">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div> 
)

export default Abonnement;