import React, {Component} from 'react'

class Field extends Component{

    render(){

        return(
            <div className={`${this.props.classN} form-groupe`}>
                <label htmlFor={this.props.name}>{this.props.label}</label>
                {this.props.elementName === 'input' ?
                    <input
                        type={this.props.type}
                        id={this.props.name}
                        placeholder={this.props.placeholder}
                        className="form-control form-control-lg"
                        required="required"
                        //value={this.props.value}
                        //onChange={e => this.props.onChange(e)}
                        name={this.props.name}
                        onChange={this.props.onChange}
                        onBlur={this.props.onBlur}
                    />
                    :
                    <textarea  
                        id={this.props.name}
                        placeholder={this.props.placeholder}
                        cols="30" 
                        rows="10" 
                        className="form-control"
                        required="required"
                        name={this.props.name}
                        onChange={this.props.onChange}
                        onBlur={this.props.onBlur}
                    />
                }
                <p className="help-block text-danger">
                   {(this.props.touched && this.props.errors) &&
                         <span>{this.props.errors}</span>
                    }
                </p>
            </div>
        )
    }
} 

export default Field;