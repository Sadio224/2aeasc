import React from 'react'

const Bande = () => (
    <div className="section-bg style-1" style={{ backgroundImage: "url( images/denomination.jpeg )"}}>
      <div className="container">
        <div className="row">
          <div className="col-lg-4 col-md-6 mb-5 mb-lg-0">
            
            <i className="fab fa-accessible-icon"></i>
            <h3>Nos Objectifs</h3>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis recusandae, iure repellat quis delectus ea? Dolore, amet reprehenderit.</p>
          </div>
          <div className="col-lg-4 col-md-6 mb-5 mb-lg-0">
            <i className="fab fa-accusoft"></i>
            <h3>Notre Mission</h3>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis recusandae, iure repellat quis delectus ea?
              Dolore, amet reprehenderit.
            </p>
          </div>
          <div className="col-lg-4 col-md-6 mb-5 mb-lg-0">
            <span className="icon flaticon-library"></span>
            <h3>Nos Valeurs</h3>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis recusandae, iure repellat quis delectus ea?
              Dolore, amet reprehenderit.</p>
          </div>
        </div>
      </div>
    </div>
)

export default Bande;