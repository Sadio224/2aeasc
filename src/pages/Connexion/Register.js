import React from 'react'
import { Link } from "react-router-dom"

const Register = () =>(
    <div>
        <div className="site-section ftco-subscribe-1 site-blocks-cover pb-4" style={{backgroundImage: "url( images/bg_1.jpg )"}}>
            <div className="container">
                <div className="row align-items-end justify-content-center text-center">
                    <div className="col-lg-7">
                        <h2 className="mb-0">Adhésion</h2>
                        <p>Remplissez le formulare d'adhésion ci-dessous.</p>
                    </div>
                </div>
            </div>
        </div> 
    

        <div className="custom-breadcrumns border-bottom">
            <div className="container">
                <Link to="/">Home</Link>
                <span className="mx-3 icon-keyboard_arrow_right"></span>
                <span className="current">Adhésion</span>
            </div>
        </div>

        <div className="site-section">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-5">
                        <div className="row">
                            <div className="col-md-12 form-group">
                                <label for="username">Nom&amp;Prenom</label>
                                <input type="text" id="username" className="form-control form-control-lg" />
                            </div>
                            <div className="col-md-12 form-group">
                                <label for="email">Email</label>
                                <input type="email" id="email" className="form-control form-control-lg" />
                            </div>
                            <div className="col-md-12 form-group">
                                <label for="session">Session</label>
                                <select className="form-control form-control-lg">
                                <option value="2010">2010</option>
                                <option value="2009">2009</option>
                                <option value="2008">2008</option>
                                <option value="2007">2007</option>
                                <option value="2006">2006</option>
                                <option value="2005">2005</option>
                                <option value="2004">2004</option>
                                </select> 
                            </div>
                            <div className="col-md-12 form-group">
                                <label for="pword">Mot de passe</label>
                                <input type="password" id="pword" className="form-control form-control-lg" />
                            </div>
                            <div className="col-md-12 form-group">
                                <label for="pword2">Confirmez le mot de passe</label>
                                <input type="password" id="pword2" className="form-control form-control-lg" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <input type="submit" value="Envoyer" className="btn btn-primary btn-lg px-5" />
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
)

export default Register;