import React from 'react'

import Activity from './Activity'

import img1 from '../../asset/images/img3.jpg'

const activities = [
    {image: img1, titre: 'Culture', description: 'Lancement officiel de la JSSC de l Aseguim', slug: '/'},
    {image: img1, titre: 'Accueil des nouveaux', description: 'Arrivée des nouveaux étudiants guinéens au Maroc', slug: '/'},
    {image: img1, titre: 'Collecte de fond', description: 'How To Create Mobile Apps Using Ionic', slug: '/'},
    {image: img1, titre: 'Assemblée', description: 'How To Create Mobile Apps Using Ionic', slug: '/'}
]

const Activities = () => (
    <div className="site-section" style={{marginTop:"-20px"}}>
        <div className="container">
            <div className="row mb-5 justify-content-center text-center">
                <div className="col-lg-7 mb-5">
                    <h2 className="section-title-underline mb-3">
                        <span style={{fontWeight:800}}>Activités récentes</span>
                    </h2>
                    <p>Association des Anciens Eleves et Sympathisants du College Sangoyah</p>
                </div>
            </div>

            <div className="row">
                <div className="col-12">
                    <div className="owl-slide-3 owl-carousel">
                        {activities.map((activity, index) =>{
                            return <Activity {...activity} key={index} />
                        })}
                    </div>
                </div>
            </div>            
        </div>
    </div>
)

export default Activities;