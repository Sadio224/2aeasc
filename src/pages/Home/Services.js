import React from 'react'

import Service from './Service'

const services = [
    {icon: 'flaticon-mortarboard', titre: 'Education', description: 'La formation etant la base de la reussite de la jeunesse guinéenne et de la guinée en particulier', slug: '/'},
    {icon: 'fa fa-ambulance', titre: 'L’assistance sociale et santé', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi hendrerit elit', slug: '/'},
    {icon: 'fa fa-hand-peace', titre: 'La prévention et la gestion des conflits', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi hendrerit elit', slug: '/'},
    {icon: 'fa fa-envira', titre: 'La protection de l’environnement', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi hendrerit elit', slug: '/', md: 'col-lg-6'},
    {icon: 'fa fa-futbol', titre: 'La Culture et le Sport', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi hendrerit elit', slug: '/', md: 'col-lg-6'}
]

const Services = () => (
    <div className="site-section">
        <div className="container">
            <div className="row mb-5 justify-content-center text-center">
                <div className="col-lg-4 mb-5">
                    <h2 className="section-title-underline mb-5">
                    <span>Nos 5 Piliers</span>
                    </h2>
                </div>
            </div>
            <div className="row" style={{marginTop: "-100px"}}>
                {services.map((service, index) => {
                    return <Service {...service} key={index} />
                })}
            </div>
        </div>
    </div>

)

export default Services;