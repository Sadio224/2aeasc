import React from 'react'
import { Link } from 'react-router-dom'

const Pilier = ({icon, titre, description, slug, md="col-lg-4"}) =>(
    <div className={`${md} col-md-6 mb-4 mb-lg-0`} style={{marginTop: "100px"}} >
        <div className="feature-1 border">
            <div className="icon-wrapper bg-primary">
                <span className={`${icon} text-white`}></span>
            </div>
            <div className="feature-1-content">
                <h2>{titre}</h2>
                <p>{description}</p>
                <p><Link to={slug} className="btn btn-primary px-4 rounded-0">Lire plus</Link></p>
            </div>
        </div>
    </div>
)

export default Pilier;