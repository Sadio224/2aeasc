import React from 'react'
import { Link } from 'react-router-dom'

const Activity = ({image, titre, description, slug}) =>(
    <div className="course-1-item">
        <figure className="thumnail">
            <Link to={slug}><img src={image} alt="Image1" className="img-fluid1" /></Link>
            <div className="category"><Link to={slug}><h3>{titre}</h3></Link></div>  
        </figure>
        <div className="course-1-content pb-2">
            <h2>{description}</h2>
            <div className="rating text-center mb-3">
                <span className="icon-star2 text-warning"></span>
                <span className="icon-star2 text-warning"></span>
                <span className="icon-star2 text-warning"></span>
                <span className="icon-star2 text-warning"></span>
                <span className="icon-star2 text-warning"></span>
            </div>
        </div>
    </div>
)

export default Activity;