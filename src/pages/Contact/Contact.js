import React, {Component} from 'react'
import { Link } from "react-router-dom"
import { withFormik } from 'formik'
import * as Yup from 'yup'

import Field from '../../helpers/Field'

const fields = {
    sections: [
        [
            {name: 'fname', label: 'Nom', elementName: 'input', type: 'text', placeholder: 'nom*', classN: 'col-md-6 form-group'},
            {name: 'lname', label: 'Prénom', elementName: 'input', type: 'text', placeholder: 'prenom*', classN: 'col-md-6 form-group'},
            {name: 'email', label: 'Email', elementName: 'input', type: 'email', placeholder: 'email*', classN: 'col-md-6 form-group'},
            {name: 'tel', label: 'Tel', elementName: 'input', type: 'number', placeholder: 'telephone*', classN: 'col-md-6 form-group'}
        ],
        [
            {name: 'message', label: 'Message', elementName: 'textarea', type: 'text', placeholder: 'votre message*', classN: 'col-md-12 form-group'}
        ]
    ]
}

class Contact extends Component{
    
    render(){
        return(
            <div>
                <div className="site-section ftco-subscribe-1 site-blocks-cover pb-4" style={{backgroundImage: "url( images/bg_1.jpg )"}}>
                    <div className="container">
                        <div className="row align-items-end">
                            <div className="col-lg-7">
                                <h2 className="mb-0">Contact</h2>
                                <p>Association des Anciens Eleves et Sympathisants du College Sangoyah.</p>
                            </div>
                        </div>
                    </div>
                </div> 
            

                <div className="custom-breadcrumns border-bottom">
                    <div className="container">
                        <Link to="/">Home</Link>
                        <span className="mx-3 icon-keyboard_arrow_right"></span>
                        <span className="current">Contact</span>
                    </div>
                </div>

                <div className="site-section">
                    <form onSubmit={this.props.handleSubmit} >
                    <div className="container">    
                        {fields.sections.map((section, sectionIndex) => {
                            return (
                                <div className="row" key={sectionIndex}>
                                    {section.map((field, i) => {
                                        return (
                                            <Field
                                                { ...field }
                                                key={i}
                                                value={this.props.values[field.name]}
                                                name={field.name}
                                                onChange={this.props.handleChange}
                                                onBlur={this.props.handleBlur}
                                                touched={this.props.touched[field.name]}
                                                errors={this.props.errors[field.name]}
                                            />
                                        )
                                    })}
                                </div>
                            )
                        })}
                        <div className="row">
                            <div className="col-12">
                                <input
                                    type="submit" 
                                    value="Envoyer" 
                                    className="btn btn-primary btn-lg px-5"
                                />
                            </div>
                        </div>
                    </div>
                    </form>
                </div>  
            </div>
        )
    }
}
    
export default withFormik({
    mapPropsToValues: () => ({
        fname: '',
        lname: '',
        email: '',
        tel: '',
        message: '',
    }),
    validationSchema: Yup.object().shape({
        fname: Yup.string().required('Vous devez donner votre nom'),
        lname: Yup.string().required('Vous devez donner votre prénom'),
        email : Yup.string().email('Email non valide').required('Vous devez donner votre email'),
        message : Yup.string()
            .min(100, 'Plus de detail')
            .max(500, 'Assez parler merci!!!')
            .required('Dites quelque chose'),
    }),
    handleSubmit: (values, {setSubmitting}) => {
        alert("Vous avez soumis le formulaire");
    }
})(Contact);