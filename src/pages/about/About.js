import React, { Component } from 'react'

import { Link } from "react-router-dom"
import './About.css'

import ObjectifMissionValeur from '../../helpers/ObjectifMissionValeur'
import Abonnement from '../../helpers/Abonement'
import BureauExecutif from './BureauExecutif'

class About extends Component{

    render(){

        return(
            <div className="site-wrap">
                <div className="site-section ftco-subscribe-1 site-blocks-cover pb-4" style={{backgroundImage: "url( images/bg_1.jpg )"}}>
                    <div className="container">
                        <div className="row align-items-end">
                            <div className="col-lg-7">
                            <h2 className="mb-0">A Propos De Nous</h2>
                            <p>Association des Anciens Eleves Et Sympathisants du College Sangoyah.</p>
                            </div>
                        </div>
                    </div>
                </div> 
    

                <div className="custom-breadcrumns border-bottom">
                    <div className="container">
                        <Link to="/">Home</Link>
                        <span className="mx-3 icon-keyboard_arrow_right"></span>
                        <span className="current">About Us</span>
                    </div>
                </div>

                <div className="container pt-5 mb-5">
                    <div className="row">
                        <div className="col-lg-4">
                            <h2 className="section-title-underline">
                              <span>Histoirique Du 2AE2SC</span>
                            </h2>
                        </div>
                        <div className="col-lg-4">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, iure dolorum! Nam veniam tempore tenetur aliquam architecto, hic alias quia quisquam, obcaecati laborum dolores. Ex libero cumque veritatis numquam placeat?</p>
                        </div>
                        <div className="col-lg-4">
                            <p>Nam veniam tempore tenetur aliquam
                            architecto, hic alias quia quisquam, obcaecati laborum dolores. Ex libero cumque veritatis numquam placeat?</p>
                        </div>
                    </div>
                </div> 

                <div className="site-section">
                    <div className="container">
                        <div className="row mb-5">
                            <div className="col-lg-6 mb-lg-0 mb-4">
                                <img src="images/logo2.jpeg" alt="Image1" className="img-fluide" /> 
                            </div>
                            <div className="col-lg-5 ml-auto align-self-center">
                                <h2 className="section-title-underline mb-5">
                                    <span>Objectifs</span>
                                </h2>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. At itaque dolore libero corrupti! Itaque, delectus</p>
                                <ol className="ul-check primary list-unstyled">
                                <li>Modi sit dolor repellat esse! Sed necessitatibus </li>
                                <li>itaque libero odit placeat nesciunt, voluptatum   </li>
                                <li>Modi sit dolor repellat esse! Sed necessitatibus </li>
                                <li>itaque libero odit placeat nesciunt, voluptatum   </li>
                                <li>Modi sit dolor repellat esse! Sed necessitatibus </li>
                                <li>itaque libero odit placeat nesciunt, voluptatum   </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <ObjectifMissionValeur />

                <BureauExecutif />
                
                <Abonnement />

            </div>
        )
    }
}

export default About;