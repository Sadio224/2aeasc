import React from 'react'

import img1 from '../../asset/images/barry.jpg'
import MembreDuBureau from '../../helpers/MembreDuBureau'

const membres = [
    {image: img1, nom: 'Abdoulaye Sadio BARRY', poste: 'Sécretaire général', profession: 'Developpeur full stack, developpeur react. PDG de XYZ.Society.'},
    {image: img1, nom: 'Abdoulaye Sadio BARRY', poste: 'Sécretaire général', profession: 'Developpeur full stack, developpeur react. PDG de XYZ.Society.'},
    {image: img1, nom: 'Abdoulaye Sadio BARRY', poste: 'Sécretaire général', profession: 'Developpeur full stack, developpeur react. PDG de XYZ.Society.'},
    {image: img1, nom: 'Abdoulaye Sadio BARRY', poste: 'Sécretaire général', profession: 'Developpeur full stack, developpeur react. PDG de XYZ.Society.'},
    {image: img1, nom: 'Abdoulaye Sadio BARRY', poste: 'Sécretaire général', profession: 'Developpeur full stack, developpeur react. PDG de XYZ.Society.'},
    {image: img1, nom: 'Abdoulaye Sadio BARRY', poste: 'Sécretaire général', profession: 'Developpeur full stack, developpeur react. PDG de XYZ.Society.'},
]

const Membres = () => (
    <div className="site-section" style={{marginTop:"-20px"}}>
        <div className="container">
            <div className="row mb-5 justify-content-center text-center">
                <div className="col-lg-7 mb-5">
                    <h2 className="section-title-underline mb-5">
                        <span>Bureau Exécutif <script>document.write(new Date().getFullYear());</script> </span>
                    </h2>
                </div>
            </div>

            <div className="row">
                
                        {membres.map((membre, index) =>{
                            return <MembreDuBureau {...membre} key={index} />
                        })}
                   
            </div>            
        </div>
    </div>
)

export default Membres;