import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom"

import './App.css';

//Pages

import PageWrapper from './components/PageWrapper'
import Home from './pages/Home/Home'
import About from './pages/about/About'
import Register from './pages/Connexion/Register'
import Login from './pages/Connexion/Login'
import Contact from './pages/Contact/Contact'

function App() {
  
  return (
    <Router>
      <PageWrapper>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/register" component={Register} />
        <Route path="/login" component={Login} />
        <Route path="/contact" component={Contact} />
      </PageWrapper>
    </Router>
  );
}

export default App;

